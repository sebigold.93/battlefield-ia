package ia.battle.util;

import ia.battle.core.FieldCell;

public class Node {
	private int g;
	private int h;
	private Node parent;

	private FieldCell cell;
	
	public Node(FieldCell cell) {
		this.cell = cell;
	}
	
	public int getF() {
		return g + h;
	}

	public int getG() {
		return g;
	}

	public void setG(int g) {
		this.g = g;
	}

	public int getH() {
		return h;
	}

	public void setH(int h) {
		this.h = h;
	}

	public Node getParent() {
		return parent;
	}

	public void setParent(Node parent) {
		this.parent = parent;
	}

	public int getX() {
		return this.cell.getX();
	}

	public int getY() {
		return this.cell.getY();
	}
	
	public FieldCell getFieldCell() {
		return this.cell;
	}

	public boolean equals(Object obj) {
		Node other = (Node)obj;
		
		return this.getX() == other.getX() && this.getY() == other.getY();
	}
	
	public String toString() {
		return "[" + this.getX() + ", " + this.getY() + "]";
	}
}
