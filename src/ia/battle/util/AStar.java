package ia.battle.util;

import java.util.ArrayList;
import java.util.Collections;

import ia.battle.core.FieldCell;
import ia.battle.core.FieldCellType;

public class AStar {

	private FieldCell[][] map;
	private ArrayList<Node> closedNodes, openNodes;
	private Node origin, destination;

	public AStar(FieldCell[][] map) {
		this.map = map;
	}

	public ArrayList<FieldCell> findPath(FieldCell origin, FieldCell destination) {
		closedNodes = new ArrayList<Node>();
		openNodes = new ArrayList<Node>();

		this.origin = new Node(origin);
		this.destination = new Node(destination);

		Node currentNode = this.origin;
		while (!currentNode.equals(this.destination)) {
			processNode(currentNode);
			currentNode = getMinFValueNode();
		}

		return retrievePath();
	}

	private ArrayList<FieldCell> retrievePath() {
		ArrayList<FieldCell> path = new ArrayList<FieldCell>();
		
		Node nodeDest = new Node(this.map[this.destination.getX()][this.destination.getY()]);
				
		Node node = openNodes.get(this.openNodes.indexOf(nodeDest));
		
		while (!node.equals(this.origin)) {
			path.add(node.getFieldCell());
			node = node.getParent();
		}

		Collections.reverse(path);

		return path;
	}

	private void processNode(Node node) {

		ArrayList<Node> adj = getAdjacentNodes(node);

		openNodes.remove(node);
		closedNodes.add(node);

		for (Node n : adj) {

			if (closedNodes.contains(n))
				continue;

			//Compute the Manhattan distance from node 'n' to destination
			int h = Math.abs(destination.getX() - n.getX());
			h += Math.abs(destination.getY() - n.getY());

			//Compute the distance from origin to node 'n' 
			int g = node.getG();
			if (node.getX() == n.getX() || node.getY() == n.getY())
				g += node.getFieldCell().getCost(); 			//10;
			else
				g += node.getFieldCell().getCost() * 1.41; 		//14;

			if (!openNodes.contains(n)) {

				if (this.destination.equals(n))
					System.out.println("Im ur father");
				
				n.setParent(node);
				n.setH(h);
				n.setG(g);

				openNodes.add(n);
			} else {

				if (h + g < n.getF()) {

					n.setParent(node);
					n.setH(h);
					n.setG(g);
				}
			}
		}
	}

	private Node getMinFValueNode() {
		Node node = openNodes.get(0);

		for (Node n : openNodes)
			if (node.getF() > n.getF())
				node = n;

		return node;
	}

	private ArrayList<Node> getAdjacentNodes(Node node) {
		ArrayList<Node> adjCells = new ArrayList<Node>();

		int x = node.getX();
		int y = node.getY();

		
		if (x+1 < this.map.length && this.map[x+1][y].getFieldCellType() != FieldCellType.BLOCKED)
			adjCells.add(new Node(this.map[x+1][y]));
		
		if (y+1 < this.map[0].length && this.map[x][y+1].getFieldCellType() != FieldCellType.BLOCKED)
			adjCells.add(new Node(this.map[x][y+1]));
		
		if (x-1 >= 0 && this.map[x-1][y].getFieldCellType() != FieldCellType.BLOCKED)
			adjCells.add(new Node(this.map[x-1][y]));

		if (y-1 >= 0 && this.map[x][y-1].getFieldCellType() != FieldCellType.BLOCKED)
			adjCells.add(new Node(this.map[x][y-1]));
		
		if (x-1 >= 0 && y-1 >= 0 && this.map[x-1][y-1].getFieldCellType() != FieldCellType.BLOCKED)
			adjCells.add(new Node(this.map[x-1][y-1]));

		if (x+1 < this.map.length && y+1 < this.map[0].length && 
				this.map[x+1][y+1].getFieldCellType() != FieldCellType.BLOCKED)
			adjCells.add(new Node(this.map[x+1][y+1]));

		if (x-1 >= 0 && y+1 < this.map[0].length &&
				this.map[x-1][y+1].getFieldCellType() != FieldCellType.BLOCKED)
			adjCells.add(new Node(this.map[x-1][y+1]));

		if (y-1 >= 0 && x+1 < this.map.length &&
				this.map[x+1][y-1].getFieldCellType() != FieldCellType.BLOCKED)
			adjCells.add(new Node(this.map[x+1][y-1]));

		return adjCells;
	}
}
